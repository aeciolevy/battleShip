const express = require('express');
const app = express();
const path = require('path');
const port = 3010;
const staticPath = path.dirname(__dirname);
app.use(express.static(`${staticPath}/battleship`));
app.get('/', (req, res) => res.sendFile(`${staticPath}/battleship/index.html`));
app.listen(port, () => console.log(`server running on port ${port}`));

