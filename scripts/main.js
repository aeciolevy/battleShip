/* eslint import/extensions: 0, no-unused-vars:0 */
/*
    * VFS Student Project
    * (C) 2014 -2018 Vancouver film school
    * @name: main.js
    * @author : Aecio Levy
*/
import MapUI from './map-ui.js';
import Game from './game.js';
import Ship from './ship.js';

document.addEventListener('DOMContentLoaded', (event) => {
  // game is being created inside mapUI
  const mapUI = new MapUI(10, 10, Game, Ship);
});
