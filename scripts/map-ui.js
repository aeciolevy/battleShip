/* eslint import/extensions: 0, no-unused-vars:0 */
/*
    * VFS Student Project
    * (C) 2014 -2018 Vancouver film school
    * @name: map-ui
    * @author : Aecio Levy
    * @usage: let m = new MapUI (5, 5, gameClass, shipClass);
              m.BuildMap(parentClass)  ex.: parentClass: div.container
*/
import { playAudio } from './helper.js';

class MapUI {
  constructor (row = 10, column = 10, game, ship) {
    this.rows = row;
    this.columns = column;
    this.level = 'easy';
    this.shipNames = ['Destroyer', 'Cruises', 'BattleShip', 'Carrier'];
    this.storage = window.localStorage;
    this.BuildMap();
    this.BuildShips();
    this.BuildScoreTable();
    this.ModalControl();
    this.ScoreSection();
    this.game = new game(row, column, 4, ship);
  }
  // Build the battle field map
  BuildMap () {
    let markup = document.querySelector("#battle-field");
    let table = document.createElement("table");
    table.classList.add("field");
    for (let i = 1; i <= this.rows; i++) {
      // jquery auto close table and tr, needed to use createElement
      let tr = document.createElement("tr");
      for (let j = 1; j <= this.columns; j++){
        tr.innerHTML += `<td class="cells" data-row=${i} data-column=${j} </td>`;
      }
      table.appendChild(tr);
    }
    markup.appendChild(table);
  }

  //Build the ships
  BuildShips () {
    let sideBar = document.querySelector("#side-bar");
    for (let i = 0; i < this.shipNames.length; i++){
      let div = document.createElement("div");
      div.classList = "ui basic segment small image ship-area";
      let divChild1 = document.createElement("div");
      divChild1.classList = "ui red ribbon label";
      divChild1.innerHTML = this.shipNames[i];
      let img = document.createElement("img");
      img.classList = "image-ship";
      img.setAttribute("src", `./images/ship${i + 1}.png`);
      let divChild2 = document.createElement("div");
      divChild2.setAttribute("id", this.shipNames[i]);
      divChild2.classList = "ui red right ribbon label";
      div.appendChild(divChild1);
      div.appendChild(img);
      div.appendChild(divChild2);
      sideBar.appendChild(div);
    }

  }
  // Modal first-view
  ModalControl () {
    const btnPlaying = document.querySelector(".play");
    const form = document.querySelector(".form");
    // Event Handle
    // Hide modal
    btnPlaying.addEventListener('click', event => {
      // change menu position to fixed
      event.target.classList = "hidden";
      const instruction = document.createElement("div");
      instruction.style = "color: #9c9898; margin-bottom: 0.7rem;"
      instruction.innerHTML = "Choose the level please";
      const easy = document.createElement("button");
      easy.setAttribute("id", "level-easy");
      easy.innerHTML = "Easy";
      easy.classList = "ui button";
      const hard = document.createElement("button");
      hard.setAttribute("id", "level-hard");
      hard.innerHTML = "Hard";
      hard.classList = "ui button";
      form.appendChild(instruction);
      form.appendChild(easy);
      form.appendChild(hard);
      this.LevelButtonEvents();
    })
  }

  LevelButtonEvents () {
    // Elements to hide modal
    const menu = document.querySelector("#navigation");
    const modal = document.querySelector("#first-view");
    // Button created run setTimeout
    const easy = document.querySelector("#level-easy");
    const hard = document.querySelector("#level-hard");
    easy.addEventListener("click", event => {
      // Set flag to game started after player has chosen level
      this.started = true;
      // change menu position to fixed
      menu.classList.add("loaded");
      modal.classList.remove("show");
      modal.classList.add("hidden");
      this.game.SetLevel = 'easy';
      playAudio('play', true);
    })
    hard.addEventListener("click", event => {
      // Set flag to game started after player has chosen level
      this.started = true;
      // change menu position to fixed
      menu.classList.add("loaded");
      modal.classList.remove("show");
      modal.classList.add("hidden");
      this.game.SetLevel = 'hard';
    })
  }
  // Controler to score board section
  ScoreSection() {
    let score = document.querySelector("#score-section");
    // Event Handlers
    let scoreNav = document.querySelector(".item.score");
    scoreNav.addEventListener("click", event => {
      score.classList.remove("hidden");
      score.classList.add("show")
    })
  }

  BuildScoreTable() {
    let tBody = document.querySelector("#score-board").childNodes[3];
    Object.keys(this.storage).forEach( el => {
      // Used document.createElement because tags was auto closing
      // due to jQuery
      let tr = document.createElement("tr");
      let td = document.createElement("td");
      let h4 = document.createElement("h4");
      h4.setAttribute("class", "ui header");
      h4.innerHTML = `${el}`;
      td.appendChild(h4);
      tr.appendChild(td)
      let tdScore = document.createElement("td");
      tdScore.innerHTML += `${this.storage.getItem(el)}`;
      tr.appendChild(tdScore);
      tBody.appendChild(tr);
    });
  }


}

export default MapUI;
