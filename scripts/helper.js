/* eslint import/extensions: 0, no-unused-vars:0, class-methods-use-this: 0 */
/* eslint no-plusplus: 0, space-before-function-paren: 0, quotes: 0 */
/*
    * VFS Student Project
    * (C) 2014 -2018 Vancouver film school
    * @name: helper.js
    * @author : Aecio Levy
    * @description: General help functions
*/
// Get random number between interval inclusive min and max
export function GetRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min +1)) + min;
}
// Check if spot is available
export function CheckAvailability(row, column, obj) {
  return !obj[`${row}-${column}`].hasShip;
}
// Check if Horizontally is available, left and right
export function CheckHorizontally(shipSize, row, column, obj, direction) {
  let isEmpty = false;
  switch (direction) {
    case 'R': {
      for (let i = 1; i < shipSize; i++) {
        isEmpty = obj[`${row}-${column + i}`] ?
          obj[`${row}-${column + i}`].hasShip : undefined;
        if (isEmpty) {
          return !isEmpty;
        }
      }
      if (isEmpty === undefined) {
        return false;
      }
      return !isEmpty;
    }
    case 'L': {
      for (let i = 1; i < shipSize; i++) {
        isEmpty = obj[`${row}-${column - i}`] ?
          obj[`${row}-${column - i}`].hasShip : undefined;
        if (isEmpty) {
          return !isEmpty;
        }
      }
      if (isEmpty === undefined) {
        return false;
      }
      return !isEmpty;
    }
    default:
      return false;
  }
}
// Check if vertically is there spot free, up and down
export function CheckVertically(shipSize, row, column, obj, direction) {
  let isEmpty = false;
  switch (direction) {
    case 'U': {
      for (let i = 1; i < shipSize; i++) {
        isEmpty = obj[`${row - i}-${column}`] ?
          obj[`${row - i}-${column}`].hasShip : undefined;
        if (isEmpty) {
          return !isEmpty;
        }
      }
      if (isEmpty === undefined) {
        return false
      }
      return !isEmpty;
    }
    case 'D': {
      for (let i = 1; i < shipSize; i++) {
        isEmpty = obj[`${row + i}-${column}`] ?
          obj[`${row + i}-${column}`].hasShip : undefined;
        if (isEmpty) {
          return !isEmpty;
        }
      }
      if (isEmpty === undefined) {
        return false;
      }
      return !isEmpty;
    }
    default:
      return false;
  }
}
// Set the shipHorizontally
export function SetHorizon(row, column, Ship, obj, direction) {
  switch (direction) {
    case "R": {
      for (let i = 0; i < Ship.GetSize; i++) {
        obj[`${row}-${column + i}`].hasShip = true;
        obj[`${row}-${column + i}`].name = Ship.GetName
      }
      break;
    }
    case "L": {
      for (let i = 0; i < Ship.GetSize; i++) {
        obj[`${row}-${column - i}`].hasShip = true;
        obj[`${row}-${column - i}`].name = Ship.GetName
      }
      break;
    }
    default:
      break;
  }
}
// Positioning the ship vertically
export function SetVertical(row, column, Ship, obj, direction) {
  switch (direction) {
    case "U": {
      for (let i = 0; i < Ship.GetSize; i++) {
        obj[`${row - i}-${column}`].hasShip = true;
        obj[`${row - i}-${column}`].name = Ship.GetName
      }
      break;
    }
    case "D": {
      for (let i = 0; i < Ship.GetSize; i++) {
        obj[`${row + i}-${column}`].hasShip = true;
        obj[`${row + i}-${column}`].name = Ship.GetName;
      }
      break;
    }
    default:
      break;
  }
}
// To set the ship location
export function SetLocation(structure, Ship, row, column, ...z) {
  let obj = {};
  // Fill the object with check results
  for (let i = 1; i <= z.length; i++) {
    obj[i] = z[i - 1];
  }
  // get a random number from one to 1 to the number of functions
  let locationMode = GetRandomInt(1, z.length);
  // Check if the mode chosen is available
  let mode = obj[locationMode];
  // if not choose another mode until it returns true
  while (!mode) {
    locationMode = GetRandomInt(1, z.length);
    console.log('locationMode: ', locationMode);
    mode = obj[locationMode]
  }
  // Positioning the ship with the random mode chosen
  switch (locationMode) {
    case 1: {
      this.SetHorizon(row, column, Ship, structure, "R");
      break;
    }
    case 2: {
      this.SetHorizon(row, column, Ship, structure, "L");
      break;
    }
    case 3: {
      this.SetVertical(row, column, Ship, structure, "U");
      break;
    }
    case 4: {
      this.SetVertical(row, column, Ship, structure, "D");
      break;
    }
    default:
      break;
  }
}

export function updateText(id, value) {
  let element = document.querySelector(`#${id}`);
  element.innerHTML = value;
}

export function BlurSunkShip(name) {
  const el = document.querySelector(`#${name}`);
  const img = el.previousElementSibling;
  img.style.filter = "blur(3px) grayscale(20%)";
}

export function calculateScore(missiles, hits, level) {
  let bonus = 0;
  if (level === 'easy') {
    bonus = 5;
  } else {
    bonus = 15;
  }
  let score = 10 * (missiles/10 * bonus) * hits;
  return score;
}

export function flashMessage(message) {
  let msg = document.querySelector('#flash-message');
  msg.innerHTML = message;
  setTimeout( () => msg.innerHTML = "", 3800);
}

export function showScore(name, score) {
  let div = document.querySelector("#win");
  let h2 = document.createElement("h2");
  h2.classList = "ui header center aligned splash";
  h2.innerHTML = `${name} - ${score} points`;
  div.appendChild(h2);
}

export function playAudio(effect, loop, stop) {
  let path = '';
  switch (effect) {
    case 'play':
      let elem = document.querySelector("#song-play");
      path = '../audio/MUS_Game_Play.mp3';
      elem.setAttribute("src", path);
      let stream = elem.play();
      elem.loop = true;
      if (stop) {
        // Avoid chrome error
        // since the audio is async
        // use a promise to pause it.
        stream.then( () => {
          elem.pause();
          elem.currentTime = 0;
        })
      }
      break;
    case 'splash':
      path = '../audio/SFX_Splash.ogg';
      break;
    case 'Destroyer':
      path = '../audio/SFX_Small_explosion.mp3';
      break;
    case 'Cruises':
      path = '../audio/SFX_medium_explosion.ogg';
      break;
    case 'BattleShip':
      path = '../audio/SFX_Large_explosion.ogg';
      break;
    case 'Carrier':
      path = '../audio/SFX_Large_explosion.ogg';
      break;
    case 'Win':
      path = '../audio/SFX_Explosion_Fireworks_01.ogg';
      break;
    default:
      break;
  }
  if (effect != 'play') {
    const audio = new Audio(path);
    audio.play();
  }

}
