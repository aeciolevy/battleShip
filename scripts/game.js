/* eslint import/extensions: 0, no-unused-vars:0, class-methods-use-this: 0 */
/* eslint no-plusplus: 0, space-before-function-paren: 0, quotes: 0 */
/*
    * VFS Student Project
    * (C) 2014 -2018 Vancouver film school
    * @name: game.js
    * @author : Aecio Levy
*/
// import as functions with namespacing binding
import * as help from "./helper.js";

const LEVEL = {
  easy: 50,
  hard: 32
}

class Game {
  constructor(rows = 10, columns = 10, shipNum, ship) {
    this.rows = rows;
    this.columns = columns;
    this.structure = {};
    this.level = 'easy';
    this.shipNum = shipNum;
    this.ShipControl = ship;
    this.ships = {};
    this.status = {}
    this.storage = window.localStorage;
    this.playerName = "Player 1;"
    this.init(false);
  }
  // initializing the game
  init (restart) {
    this.structure = {};
    this.ships = {};
    this.CreateMapStructure();
    this.CellsEventListener(restart);
    this.CreateShip();
    this.status = { missiles: LEVEL[this.level], hits: 0, score: 0 };
    help.updateText('missiles', this.status.missiles);
    help.updateText('hits', this.status.hits);
    help.updateText('score', this.status.score);
  }
  //Set game Level
  set SetLevel(level) {
    this.level = level;
    this.status.missiles = LEVEL[this.level];
    help.updateText('missiles', this.status.missiles);
  }
  // Creating Ships
  CreateShip () {
    const names = ["Destroyer", "Cruises", "BattleShip", "Carrier"];
    for (let i = 0; i < this.shipNum; i++) {
      const ship = new this.ShipControl(i + 2, names[i]);
      this.ships[names[i]] = ship;
      this.PositioningShips(ship);
    }
  }
  // Create Map Data structure
  CreateMapStructure() {
    for (let i = 1; i <= this.rows; i++) {
      for (let j = 1; j <= this.columns; j++) {
        this.structure[`${i}-${j}`] = {
          hasShip: false,
          clicked: false
        };
      }
    }
  }
  // Event listener to all cells on the map
  CellsEventListener(restart) {
    const cells = document.querySelectorAll(".cells");
    Object.values(cells).forEach((elem) => {
      elem.classList = "cells";
      // avoid duplicate event listener
      if (!restart) {
        elem.addEventListener("click", this.HandleShoots.bind(this));
      }
    });
  }
  // Event listener handle
  HandleShoots(event) {
    const { row, column } = event.target.dataset;
    const result = this.CheckShoot(row, column);
    if (!result) {
      event.target.classList.add("wrong");
    } else {
      event.target.classList.add("hit");
    }
  }
  // To position ship
  PositioningShips(Ship) {
    // generate a random number to row
    let row = help.GetRandomInt(1, this.rows);
    // generate a random number to column
    let column = help.GetRandomInt(1, this.columns);
    // Check if the cell is available
    let isAvailable = help.CheckAvailability(row, column, this.structure);
    let horizonR = help.CheckHorizontally(
      Ship.GetSize,
      row,
      column,
      this.structure,
      "R"
    );
    let horizonL = help.CheckHorizontally(
      Ship.GetSize,
      row,
      column,
      this.structure,
      "L"
    );
    let verticalU = help.CheckVertically(
      Ship.GetSize,
      row,
      column,
      this.structure,
      "U"
    );
    let verticalD = help.CheckVertically(
      Ship.GetSize,
      row,
      column,
      this.structure,
      "D"
    );
    // if the cell is not available OR cell horizontally right and left
    // vertically up and down are not available, calculate a new position
    while (!isAvailable || (!horizonR && !horizonL && !verticalD && !verticalU)) {
      row = help.GetRandomInt(1, this.rows);
      column = help.GetRandomInt(1, this.columns);
      isAvailable = help.CheckAvailability(row, column, this.structure);
      horizonL = help.CheckHorizontally(Ship.GetSize, row, column, this.structure, "L");
      horizonR = help.CheckHorizontally(Ship.GetSize, row, column, this.structure, "R");
      verticalU = help.CheckVertically(Ship.GetSize, row, column, this.structure, "U");
      verticalD = help.CheckVertically(Ship.GetSize, row, column, this.structure, "D");
    }
    help.SetLocation(
      this.structure,
      Ship,
      row,
      column,
      horizonR,
      horizonL,
      verticalU,
      verticalD
    );
  }
  // Check status and check which modal is gonna be showed
  ShowModals() {
    let id = '';
    if (this.status.missiles <= 0 && !this.AllSunks()) {
      id = 'game-over';
    } else if (this.status.missiles >= 0 && this.AllSunks()) {
      id = 'win';
    }
    if (id !== '') {
      let button;
      const toShow = document.querySelector(`#${id}`);
      const menu = document.querySelector("#navigation");
      if (id === 'game-over') {
        button = document.querySelector('.play-again');
      } else if (id === 'win') {
        button = document.querySelector('#win-button');
        this.storage.setItem(this.playerName, this.status.score);
        help.playAudio('Win');
        help.showScore(this.playerName, this.status.score);
      }
      help.playAudio('play', false, true);
      menu.classList.remove('loaded');
      toShow.classList.remove('hidden');
      toShow.classList.add('show');
      button.addEventListener("click", (e) => {
        menu.classList.add('loaded');
        toShow.classList.remove('show');
        toShow.classList.add('hidden');
        // Delete the score of the page
        if (id === "win"){
          let div = document.querySelector("#win");
          let h2 = div.childNodes[5];
          div.removeChild(h2);
        }
        //Play game play song
        help.playAudio('play', true);
        // Argument is true to make sure to NOT create one more
        // event listenet to the cells
        this.init(true);
      });
    }

  }
  // Check if all ships sunk
  AllSunks () {
    const sunk = Object.values(this.ships).reduce((curr, prev) => {
      return curr && prev.HasSunk
    }, true);
    return sunk;
  }
  // Handle events after click on one cell;
  CheckShoot(row, column) {
    // Get the cell clicked
    const shoot = this.structure[`${row}-${column}`];
    // Get the player name
    if (this.status.missiles === LEVEL[this.level]){
      this.playerName = document.querySelector("#player-name").value;
      if (this.playerName === '') {
        this.playerName = 'Player';
      }
    }
    if (!shoot.clicked) {
      shoot.clicked = true;
    } else {
      // Send a flash message to user knows he already click this cell
      help.flashMessage('You already clicked here and lose one missile 🤷🏼‍')
      return false;
    }
    this.status.missiles -= 1;
    help.updateText('missiles', this.status.missiles);
    if (shoot.hasShip) {
      this.status.hits += 1;
      help.updateText('hits', this.status.hits);
      this.status.score +=
        help.calculateScore(this.status.missiles, this.status.hits, this.level);
      help.updateText('score', this.status.score);
      const { name } = shoot;
      // Update ship hit
      this.ships[name].Hit(name);
      // Blur image if ship is sunk
      this.ShowModals();
      if (this.ships[name].HasSunk) {
        help.BlurSunkShip(name);
      }
      return true;
    } else {
      help.playAudio('splash')
    }
    this.ShowModals();
    return false;
  }
}

export default Game;
