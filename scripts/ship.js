/*
    * VFS Student Project
    * (C) 2014 -2018 Vancouver film school
    * @name: Ships
    * @author : Aecio Levy
    * @usage: let s = new Ship (3, "Cruiser");
*/
import { playAudio } from './helper.js';

class Ship  {
  constructor (theSize, theName = "Destroyer") {
    this.size = theSize;
    this.name = theName;
    this.hits = theSize;
    this.init();
  }

  get HasSunk () { return (this.hits == 0); }

  get GetName () { return this.name }

  get GetSize () { return this.size }

  init () {
    // Initialize the hits
    const ship = document.querySelector(`#${this.name}`);
    ship.innerHTML = this.size;
    // Make sure the is no style effect to the images
    let sibling = ship.parentNode.childNodes[1];
    sibling.style = "filter: none";
  }

  Hit (name) {
    if (!this.HasSunk) {
      playAudio(this.name);
      this.hits -= 1;
    }
    const ship = document.querySelector(`#${name}`);
    ship.innerHTML = this.hits;
  }

}

export default Ship;
